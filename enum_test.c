#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

enum e64 {
	e64_u64 = 0xfffffffffffffff0ULL,
	e64_u32 = 0xfffdff0U,
};

enum e32 {
	e32_u32 = 0xffffff0,
	e32_i32 = -6,
};

int main(int argc, char **argv)
{
	enum e64 e64_var;
	enum e32 e32_var;

	printf("e64_u64=%llx sz=%zd, e64_u32=%llx sz=%zd\n",
		   (long long)e64_u64,
		   sizeof(e64_u64),
		   (long long)e64_u32,
		   sizeof(e64_u32));
	printf("e32_u32=%x sz=%zd, e32_i32=%d sz=%zd\n",
		   e32_u32,
		   sizeof(e32_u32),
		   e32_i32,
		   sizeof(e32_i32));

	e64_var = e64_u32;
	e32_var = e64_u32;

	printf("e64_var sz=%zd e32_var sz=%zd\n", sizeof(e64_var), sizeof(e32_var));

	return 0;
}
