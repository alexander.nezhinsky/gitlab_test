# Coding Style

This is a short document describing preferred coding style.


## Based on kernel coding style

It is largely based on the coding style guidelines for linux kernel.
You are advised to read it (at least for anthropological reasons).

This document contains all kernel coding style recommendations,
formulated in plain language with arrogant and offensive wording omitted.

## Coding Style rules

### Indentation

Indentation is Tab-based. Tabs are 8 characters long.

Longer indents coupled with restrictions on the line length should encourage you
to keep low level of nesting and use reasonably short function and variable names.

### Switch indentation

Always align "switch" and "case" statements at the same column.

### No multiple statements on the same line

Never use this:
``` cpp
        if (condition) do_something();

```

### No multiple assignments on the same line

Never use this:
``` cpp
        counter = num_entries = n;

```

Always do this:
``` cpp
        counter = n;
        num_entries = n;

```

### No trailing whitespace

Just no. Including empty lines containing only indents.

## Breaking long lines

### Maximal line length 

We relax the strict 80 characters limitation, replacing it with a fuzzy 90-100
characters recommendation. Really long lines, around 120-140 characters are
strongly discouraged. 

90 characters with some tolerance for extras is the default setting of clang-format.

### Breaking function declarations and invocations

Functions parameters in declarations and arguments in invocations may be left on
the same line if they do not exceed the above limitation of 90-100 characters.
Otherwise they should be broken by placing each parameter/argument after the first one
on a separate line indented by a single tab.
``` cpp
static struct useful_entry *useful_manager_get_entry(struct useful_manager *manager,
	void *user_context,
	enum useful_type type)
{
	struct useful_entry *new_entry;

	new_entry = useful_manager_allocate_entry(manager,
		useful_get_size(type),
		ALLOC_REGULAR);
	. . .
}

```

### Pointer types

In definitions involving pointer types, * is put close to the variable name.

Example:
``` cpp
        struct my_buffer *buf;
```

### Alignment of struct fields and local variables

Struct fields and local variable are unaligned, delimited with just a single space.

Example:
``` cpp
    struct my_ctrl {
            struct my_param param;
            struct my_buffer *buf;
            int fd;
    };

    void my_func()
    {
            struct my_ctrl *ctrl;
            int i, err;
            . . .
    }
```

### Opening braces

Same line scope opening braces:
``` cpp
    if (x > 0) {
            do_something(x);
            do_more();
    }
```

Exception is made for macro-based iterators which always tend to be much longer 
than standard ```for``` and ```while``` constructs: 
``` cpp
        list_for_each_safe(&ctxt->cmd_manager.pending_cmd_list, cur_cmd, next_cmd, ctxt_list_node)
        {
                do_something(cur_cmd);
        }
```

Next line function opening braces:
``` cpp
        void func(char *str)
        {
                do_something(str);
        }
```

### Continuation of statements after braces

Statement continuations follow closing braces on the same line:
``` cpp
        if (x == a) {
                do_something(x, a);
                do_more();
        } else { /* same line after closing brace */
                make_amends(x, a);
        }

        do {
                do_stuff(x);
                --x;
        } while (x > 0); /* same line after closing brace */
```

### No braces for single line clause statements

``` cpp
        if (x != 0)
                do_something(x);
```

When ```else``` is present keep the same style for both clauses:
``` cpp
        if (x != 0) {
                do_something(x);
        } else {
                check_stuff(x);
                do_otherwise(x);
        }
```

### Spaces after keywords

Always use spaces after: if, while, for, do, switch, case
``` cpp
        if (x != 0) { . . .
        while (x != 0) { . . .
        do { . . .
        for (i = 0; i < n; i++) { . . .
        switch (x) { . . .
        case STATE_IDLE:
```

### No spaces after keywords

Never use spaces after: sizeof, typeof, alignof, __attribute__
``` cpp
        sizeof(x)
        typeof(x)
        alignof(my_params)
        __attribute__((unused))
```

### Right-justified pointer sign

In declaration of pointer data or return type always put the * char close to
the name of data or function;
``` cpp
        struct my_param *get_param(struct my_entry *entry)
        {
                char *string;
                . . .
```

### Single space around binary and ternary operators

Always put a single space on each side of the following:
```=  +  -  <  >  *  /  %  |  &  ^  <=  >=  ==  !=  ?  :```

### No space after unary operators

Never put spaces after the following:
```&  *  +  -  ~  !```

and no space before the postfix and after the prefix increment & decrement operators:
```++  --```

no space around the structure member operators:
```. ->```

### Header conditional preprocessing symbols

Header files are wrapped with preprocessor symbols definition
symbol names should be of form ```_<NAME_IN_CAPITALS>_H_```

Example: file ```common/my_ctrl.h``` should define symbol ```_COMMON_MY_CTRL_H_```

### Naming of constructor/destructor operations

Basic constructor/destructor-like operations may have different semantics
for different types. There are recurrent patterns which should be recognized
and reflected in the function names:

  - Allocation, potentially with some partial basic initialization: *alloc/free*.
    Example:
``` cpp
        struct my_ctrl *my_ctrl_alloc(void);
        void my_ctrl_free(struct my_ctrl *);
```

  - Full (or almost full) initialization in available memory: *init/cleanup*.
    Example:
``` cpp
        void my_ctrl_init(struct my_ctrl *);
        void my_ctrl_cleanup(struct my_ctrl *);
```

  - Allocation and initialization: *create/destroy*.
    Example:
``` cpp
        struct my_ctrl *my_ctrl_create(void);
        void my_ctrl_destroy(struct my_ctrl *);
```

**!!!** *The main reason of writing this is to avoid name couples which don't match
and may signal wrong semantics, like init/free, alloc/destroy etc.*

Sometimes create/destroy or init/cleanup operations may have clear open/close semantics,
in which cases the functions should be named appropriately.


### Using typedef

We keep kernel style prohibition for using typedef, especially with structs!

This should never happen:
``` cpp
    typedef struct my_ctrl my_ctrl_t;
```

Two exceptions exist:

  - it is ok to typedef "void *" especially as a "handle" type when exporting something really opaque.
    Example:
``` cpp
    typedef void *reg_mem_h;
```

  - redeclaring basic integer types for brevity
    Example:
``` cpp
    typedef unsigned char u8;
    typedef int rc; /* universal return code */
```

  - sometimes (and you should think twice if it is really necessary), basic integer types
    may be typedefed to stress their semantic relevance within a certain module/domain.
    Example:
``` cpp
    typedef uint16_t crc_t;
```

### Comment blocks before function declarations
Comment blocks put before function declarations should be used only when the function workings
really beg an explanation, which should happen rarely. 
No such comment blocks should contain the function name.
Refactoring tools usually change the actual function name leaving the name in comments intact,
so with time wrong names tend to accumulate in comments.

## Naming

### Variables and functions naming

Avoid using too short and non-descriptive names.
Avoid using too long and distracting names.

``` cpp
        int dc_nelems(struct dir_container *dc); /* bad : too short */
        int directory_container_get_number_of_directory_elements(
		struct directory_container *directory_container); /* bad : too long */
        int dir_num_elements(struct dir_container *container); /* ok? non ok? better than above! */
```

It is almost impossible to tell where the exact point of balance lies.
In the above example dir_container may be sometimes better rendered in full: ```directory_container```,
depending on various factors. Usually some simple shortcuts, like ```num```, ```usr```, ```dir```, ```sz``` etc. 
are quite acceptable, while abbreviations like ```lspc```, ```fmgr```, ```dp``` etc. are not.  

### List fields naming
When list *head* and list *node* fields are declared within some containing structs,
try to use the naming rule described below, whenever possible.

The rule is based on recoginizing two general short names of:

  1. ```NODE_entity_type``` of entity to be kept on list (struct containing the list *node*);
  2. ```HEAD_entity_type``` of entity maintaining the list (struct containing the list *head*).

Then:

  - list HEAD field is named: ```{NODE_entity_type}_list```
  - list NODE field is named: ```{HEAD_entity_type}_list_node```

Example:

If "Black Box" descriptors are to be kept on a list hosted by a "Box Manager", then:

  - ```NODE_entity_type``` = ```black_box```
  - ```HEAD_entity type``` = ```box_manager``` or just ```manager```

The declarations may look like:
``` cpp
struct box_manager {
                struct list_head   black_box_list;
                . . .
};
struct black_box_descr {
                struct list_head   manager_list_node;
                . . .
};

struct box_manager *box_mgr;
struct black_box_descr *new_box;
list_add(&box_mgr->black_box_list, &new_box->manager_list_node);
```

### Constants Naming

Constants (both macros and enums) should be named using CAPITALS.

``` cpp
#define STATE_READY 1

enum my_state {
        STATE_IDLE = 0,
        . . .
```

## Local variables

### Order of declaration

First put all auto-initialized variables.
Arrange the variables in order of access;
ry to keep the total number of local variables under 10, consider refactoring 
into few smaller functions if you end up with more.

``` cpp
void func(void *usr_data)
{
        struct my_params *params = data; /* auto-initialized first */
        struct buffer_element *element; /* in order of access */
        rc ret;

        element = buffer_element_create(params->size);
        . . . 
        ret = buffer_element_save(element, params->fd);
        . . .
}
```

### No declarations inside function code bodies 

Declaring variables inside function code (C99 style) is deprecated.

## Frequently occurring patterns

### Simple increments and decrements
When a statement consists only of incrementing or decrementing a counter-like variable,
or in any place where the result of such operation is not immediately used,
always put the increment/decrement operator before the variable, as it is known to generate
more efficient code, at least with gcc.

Example:
``` cpp
        i++; /* instead of this ... */
        ++j; /* do this ... */
        --num_nodes; /* and this */
        if (num_entries++) /* but this is another story */
```

### Retrying operations
Some actions, especially based on system calls, may be interrupted (by signals etc.)
or otherwise fail in a way which requires retry of the operation. This usually
breaks the flow into 3 branches:

  - further processing on success or other expected "normal" outcome;
  - retry after interrupt or other special error status;
  - exit on remaining non-retriable or fatal errors.

Example:
``` cpp
for (;;) {
        err = file_read(fd, buf, len);
        if (likely(!err)) /* normal path, continue after loop */
                break;
        else if (errno != EINTR) { /* retry on EINTR, fail on error */
                fprintf(stderr, "failed to read, %m\n");
                return -1;
        }
}
```

### Loops termination
If the loop is based on counter try, whenever possible, to decrement it on each step 
and exit the loop when the counter reaches zero. This check is much more efficient
than comparing against a maximal limiting value.

Simple loop example:
``` cpp
n = num_entries;
while (n--) /* order of releases is unimportant */
        entry_release(entry[n]);
return num_etries;
```

Sometimes it is possible to combine auto-decrementing index and auto-incrementing pointer.
``` cpp
for (i = 0; i < num_entries; i++) {
        entry = &entry_array[i];
        do_something(entry);
}
```

``` cpp
for (i = num_entries, entry = entry_array; i; --i, ++entry)
        do_something(entry);
```


## Automatic tools

Automated formatter should be used before any commit, followed by the style checker.
Usually none or just a few warnings are left after the formatter, so they should be 
addressed and fixed, unless there is an overwhelmingly good reason for not doing this.

## Automated style formatter

```clang-format``` tool, which is a part of clang suite can enforce kernel-like
coding style given an appropriate configuration.
This configuration is saved in file ```.clang-formt``` (default name).
Softlink to formatter app itself is created under ```coding/clang-format```.

Using for re-formatting in-place:
``` bash
./coding/clang_format -i <src_file_name>
```

To standard output:
``` bash
./coding/clang_format <src_file_name>
```

